05
----------------------

ma va j60 : 3eme phrase incorrecte ("il fait beau" , ou la date, la plupart du temps).
je py j60 : idem
fr ca 49 j90/60 : aucune phrase correcte/ j30 : illisible, dysgraphie +++ => sujet sûrement à exclure ! 
et ma 45 j30 : 2eme phrase incorrecte
be bu 44 j30 : mauvaise phrase + mauvaise orientation de la feuille (paysage) : pas fait en entier
ar ro 43 j60 : 2eme phrase nom (1 seul mot)
an go 43 j90 : 3eme phrase incorrecte
an fe 54 j30 : 3eme phrase incorrecte


-- 05-03-20

JE WI 41 : pas de BEN à J30 ; marge J60=18 ; 17,5, 17cm ; marge J90=17,3 ; 17,3 ; 17,3cm

MA AL 47 : pas de J60 ; marges J30 : 12,5 ; 16,5 ; 14,5cm ; marge J90 : 19,2 ; 19,3 ; 19,6 cm

NI RE 42 : BEN J30 pas valable ; pas de BEN J60 ; marge J90 : 13 ; 16,3 ; 16,4

PI DU 60 : pas de BEN et GAI à J 60 ; Marges J30 :  13,2 ; 11,4 ; 7,5cm ; Marge J 90 : 9,6 ; 6,4 ; 7,7cm
