15
===
- Copie : 2/3 formes mtu => normes apa globale. moins severe sur ref. 
- f. project : terminé. à envoyer. en attente des autres dossiers. 
- projet ap => se renseigner vite sur matos, et commencer rédaction du cerga dès demain sur partie intro. 
- litté : continuer articles "causaux". willacker 2019 à voir. puis santos.
- resumé : selectionner papier cruciaux, faire tri car certains papiers sont vraiment peu importants ... 
- bonus : AIC/episteme, continuer sur cette voie. 
- script R => se former à plot simple avec R base ? retoucher clean fit pour la simplifier à fonds et supprimer tye = "diag". 

12
===
- Projet f. ben / remarques =   
ma va j60 : 3eme phrase incorrecte ("il fait beau" , ou la date, la plupart du temps).  
je py j60 : idem  
fr ca 49 j90/60 : aucune phrase correcte/ j30 : illisible, dysgraphie +++ => sujet sûrement à exclure !   
et ma 45 j30 : 2eme phrase incorrecte  
be bu 44 j30 : mauvaise phrase + mauvaise orientation de la feuille (paysage) : pas fait en entier  
ar ro 43 j60 : 2eme phrase nom (1 seul mot)  
an go 43 j90 : 3eme phrase incorrecte  
an fe 54 j30 : 3eme phrase incorrecte  

11
===
- projet f. : conversion bmp ok
- projet e. : mail envoyé, bloqué côté analyse data, mais reflexion theorique tjrs souhaitable pour simplifier le déroulé théorique.
- bareme en attente pour mtu
- epistemo : paul meehl et les behavioristes = articles à lire

10
===

- Lecture de l'article de Michel 2003, extremement intéressant : l'AP gauche vhez sujet sain module la distribution du poids/shift postural vers la droite. Comme AP influe SSA, on peut se dire que dans les 2 cas on a ap qui agit sur le référentiel égocentré. Tout porte à penser que ce réferentiel/schéma corporel est crucial pour la posture. Spéculation : aires polymodale tpj impliquée dans tt égocentré + réception inputs vb + lésé dans nsu perso. Soutient vraiment idée que nsu perso pure vs extraperso pure devrait se distinguer par une atteinte de la verticale posturale.
- Attention à ne pas oublier 3 tâches la semaine prochaine :
- open science : rmd propre  + reviewing de qql + module 4 + zotero
- f. = angle calculator
- copie mtu
- r. = preparer carte concepts du cours + ex avec R graphiques (gggplot dynamique = montrer fluctuation ?)

09
===

- Reunion TD r. = explorer diapo avec infer. insister sur qqles concepts clefs.
- MTU : correction avant le 30 : 48 copies, voir bareme sur doodle.

08
===

Compte-rendu session f. :
- scan des patients dossiers j30/60/90
- quand scan incomplet, surtout si pas ben, exclusion
- nature des phrases importantes, toujours à mentionner
- regrader les scnas/screener afin de mentionner toute anomalie et potentielle exclusion
- pb : base de donnée : m. ou l. ? est ce que m. a test nécessaire ? 
- faire synchro permanente avec flora
- inverser les signes pour angle calculator et faire -21 pour les marges
- mode emploi angle calculator : ben = marge d'abord, puis espcae pour passer à texte, ou l'inverse, gai = arbre d'abord, pas d'espace pour passer à horiz, espace quand pas ligne à gauche et idem pour chaque arbre manquant, et à la fin taper sur espace pour valider et passer à un autre scan avec au préalable création de dossiers scans. 


07
===
tag = projet fleur
- Base de données de f. complétée, je remarque que 12 patients sont inclus dans la base avant 12/18, mais pour quoi pas avant ? mystère.
- Demander à e. si aphasie == oui et si edinb >= 0.6. intégrer nouveaux patients
- demander à l. ou qql d'autre data des tests, pas dispo dans data l. sinon, on pourra pas avencer sur projet e.

tag = projet ethique pour fin janvier max
- commencer litté sur lien posture-vv * ap. 
- demander pr. si matos ok pour quel mois

tag = projet body
- completer nouvelle base
- ebauche d'intro + pca 
- preparer code regression vue avec e. 

tag=litterature
- lire ap*vv
- cct structurale pivc
- sakata et co
- article pr.

