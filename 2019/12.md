This is the notebook of december. This notebook is a first trial, and will be improved with the experience to find its perfect style. 

05/12
===================

Goal of the day : 
- simulate data for the NSU project 
- make a quick recap of the hypothesis 
- session 3 McElreath 

17/12
===================

Goal : 

- Mooc module 3
- Code R reproductible pour l'analyse du projet M2, tester les nouveaux filtres. 

Idée d'hypothèse : 
- MIV lié à schéma corporel, plus impacté dans NSU BN que NB

18/12
===================

Aujourd'hui travail sur une fonction R qui exclue les outliers de la régression et affiche les différences en terme de summary et de ggplot.
Voici le script : 


clean_fit <- function(lm, data){
  
  # a) Detection des outliers
  
  cook<-data[which(cooks.distance(lm) > mean(cooks.distance(lm)) *4),] %>% 
    as_tibble() %>% 
    mutate(diag = "cookd")
  
  hat<-data[which(hatvalues(lm) > mean(hatvalues(lm)) *3),] %>% 
    as_tibble() %>% 
    mutate(diag = "hat")
  
  n = nrow(data)
  cutoff = qt(1 - 0.05 / (2*n), (n - 4))
  sdr<-data[which(abs(rstudent(lm)) > cutoff),] %>% 
    as_tibble() %>% 
    mutate(diag = "sdr")
  
  outliers_vector <-
    bind_rows(cook, hat, sdr) %>% 
    .[['subject']] %>% 
    unique(.)
  
  outliers <-
    bind_rows(cook, hat, sdr)
  
  print(outliers)
  
  
  # b) Graphique qui représente la droite de régression avec et sans les outliers
  
  # Variable dépendante et indépendante
  vd <- as.character(formula(lm)) %>% .[2] 
  vi <- ifelse((as.character(formula(lm)) %>% .[3]) == "1", "intercept",  (as.character(formula(lm)) %>% .[3]))
  
  # Coefficients de la droite de régression : 
  # Régression avec outliers
  coeff=coefficients(lm)
  
  # Régression sans outliers
  formule<-formula(lm)
  model<-lm(formule, data_no_outlier )
  
  coeff_no_outliers=coefficients(model)
  
  # Equation de la droite de regression : 
  eq = paste0("y = ", round(coeff[1],1), " + ", round(coeff[2],1), "*x ")
  eq_no_outliers = paste0("y = ", round(coeff_no_outliers[1],1), " + ", round(coeff_no_outliers[2],1), "*x ")
  
  # Graphe
  # Avec outliers
  g1<-lm %>% 
    augment %>% 
    ggplot(data =., aes_string(x = vi, y = ".fitted")) + 
    geom_line(col = "blue", size = 1.5) +
    geom_point(aes_string(x = vi, y = vd))+
    geom_point(data = outliers, col = "red", size = 8, pch = 21, aes_string(x = vi, y = vd))+
    labs(title = paste(eq, " | ", string, sep = "", collapse = NULL))+
    geom_label_repel (
      data = outliers, 
      aes_string(x = vi, y = vd, label = "subject"), size = 4, col = 'red'
    ) 
  
  # Sans outliers
  g2<-model %>% 
    augment %>% 
    ggplot(data =., aes_string(x = vi, y = ".fitted")) + 
    geom_line(col = "blue", size = 1.5) +
    geom_point(aes_string(x = vi, y = vd))+
    geom_point(data = outliers, col = "red", size = 8, pch = 21, aes_string(x = vi, y = vd))+
    labs(title = paste(eq_no_outliers, " | ", string, sep = "", collapse = NULL))
  
  g3 <- g1 +g2
  
  print(g3)
  
  assign(paste("plot_", string, sep =""), g3, envir = .GlobalEnv)
  
  
  # c) Nouveau dataframe sans outlier pour lancer la nouvelle régression 
  data_no_outlier <- 
    data %>% 
    dplyr::filter(!subject %in% outliers_vector)
  
  string <- Reduce(paste, deparse(as.formula(lm)))
  
  assign(paste("data_", string, sep =""), data_no_outlier, envir = .GlobalEnv)
  
  # d) Régression avant et après avoir enlevé les outliers
  vi<-formula(lm)
  model<-lm(vi, data_no_outlier )
  lm_no_outlier <- tidy(model)
  
  print("regression avec outliers")
  print(tidy(lm)) 
  
  print("regression sans outliers")
  print(lm_no_outlier) 
  
  assign(paste("lm_", string, sep =""), lm_no_outlier, envir = .GlobalEnv)
  
}


